﻿namespace ReverseConnectTunnelServer
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Server server = new Server();
            server.Startup();
            ManualResetEvent manualResetEvent = new ManualResetEvent(false);
            manualResetEvent.WaitOne();
        }
    }
}