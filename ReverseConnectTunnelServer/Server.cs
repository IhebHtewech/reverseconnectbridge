﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ReverseConnectTunnelServer
{
    internal class Server
    {
        public Server()
        {
            m_connections = new List<Connection>();
        }

        public async void Startup()
        {
            IPEndPoint ipEndpoint = new IPEndPoint(IPAddress.Any, m_port);
            Socket listeningSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            listeningSocket.Bind(ipEndpoint);
            listeningSocket.Listen(m_backlog);
            while (true)
            {
                Socket acceptSocket = await listeningSocket.AcceptAsync();
                Connection connection = new Connection(acceptSocket);
                connection.ConnectionClosed += OnConnectionClosed;
                m_connections.Add(connection);
            }
        }

        private void OnConnectionClosed(object sender, EventArgs args)
        {
            m_connections.Remove((Connection)sender);
        }

        private const int m_port = 5001;
        private const int m_backlog = 1000;
        private IList<Connection> m_connections;
    }
}
