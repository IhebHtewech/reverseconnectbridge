﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ReverseConnectTunnelServer
{
    internal class Connection : IDisposable
    {
        public event EventHandler<EventArgs> ConnectionClosed;

        public Connection(Socket socket)
        {
            m_disposed = false;
            m_lock = new object();
            m_serverPort = 80;
            m_clientSocket = socket;
            m_serverSocket = null;
            m_clientBuffer = new byte[4096];
            m_serverBuffer = new byte[4096];

            Initialize();
        }

        private async void Initialize()
        {
            IPEndPoint ipEndpoint = new IPEndPoint(IPAddress.Parse("196.203.79.227"), m_serverPort);
            m_serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            await m_serverSocket.ConnectAsync(ipEndpoint);

            ClientListenAsync();
            ServerListenAsync();
        }

        public async void ClientListenAsync()
        {
            int bytesRead;

            try
            {
                while (m_clientSocket.Connected)
                {
                    bytesRead = await m_clientSocket.ReceiveAsync(m_clientBuffer, SocketFlags.None);
                    if (bytesRead > 0)
                    {
                        ServerForwardAsync(bytesRead);
                    }
                    else
                    {
                        Dispose();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Dispose();
            }
        }

        private async void ClientForwardAsync(int bytesRead)
        {
            try
            {
                if (m_clientSocket.Connected)
                {
                    await m_clientSocket.SendAsync(new ArraySegment<byte>(m_serverBuffer, 0, bytesRead), SocketFlags.None);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Dispose();
            }
        }

        public async void ServerListenAsync()
        {
            int bytesRead;

            try
            {
                while (m_clientSocket.Connected)
                {
                    bytesRead = await m_serverSocket.ReceiveAsync(m_serverBuffer, SocketFlags.None);
                    if (bytesRead > 0)
                    {
                        ClientForwardAsync(bytesRead);
                    }
                    else
                    {
                        Dispose();
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Dispose();
            }
        }

        private async void ServerForwardAsync(int bytesRead)
        {
            try
            {
                if (m_serverSocket.Connected)
                {
                    await m_serverSocket.SendAsync(new ArraySegment<byte>(m_clientBuffer, 0, bytesRead), SocketFlags.None);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Dispose();
            }
        }

        public void Dispose()
        {
            lock (m_lock)
            {
                if (!m_disposed)
                {
                    m_disposed = true;
                    m_clientSocket?.Shutdown(SocketShutdown.Both);
                    m_clientSocket?.Close();
                    m_serverSocket?.Shutdown(SocketShutdown.Both);
                    m_serverSocket?.Close();
                    ConnectionClosed.Invoke(this, EventArgs.Empty);
                }
            }
        }

        private object m_lock;
        private bool m_disposed;
        private int m_serverPort;
        private Socket m_serverSocket;
        private Socket m_clientSocket;
        private byte[] m_clientBuffer;
        private byte[] m_serverBuffer;
    }
}
