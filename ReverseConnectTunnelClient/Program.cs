﻿using System.Net;
using System.Net.Sockets;

namespace ReverseConnectTunnelClient
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Client client = new Client();
            client.Startup();
            ManualResetEvent manualResetEvent = new ManualResetEvent(false);
            manualResetEvent.WaitOne();
        }
    }
}